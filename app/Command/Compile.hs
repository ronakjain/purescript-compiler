{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Command.Compile (command) where

import           Data.List (find, isSuffixOf)
import           Control.Applicative
import qualified Control.Concurrent as CC
import           Control.Concurrent.Lifted as C
import           Control.Monad
import           Control.Monad.Writer.Strict
import qualified Data.Aeson as A
import           Data.Bool (bool)
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.UTF8 as BU8
import           Data.IORef
import qualified Data.Map as M
import           Data.Text (Text)
import qualified Language.PureScript as P
import           Language.PureScript.Errors.JSON
import           Language.PureScript.Make
import qualified Options.Applicative as Opts
import qualified System.Console.ANSI as ANSI
import           System.Exit (exitSuccess, exitFailure)
import           System.Directory (doesFileExist, getCurrentDirectory)
import           System.FilePath (replaceExtension)
import           System.FilePath.Glob (glob)
import           System.IO (hPutStr, hPutStrLn, stderr, stdout)
import           System.IO.UTF8 (readUTF8FileT)
import           System.FSNotify
import Data.Time

data PSCMakeOptions = PSCMakeOptions
  { pscmInput        :: [FilePath]
  , pscmOutputDir    :: FilePath
  , pscmOpts         :: P.Options
  , pscmUsePrefix    :: Bool
  , pscmJSONErrors   :: Bool
  , pscmWatchSrc     :: Bool
  }

data BuildType = Boot | Add String | Remove String | Modify String

-- | Argumnets: verbose, use JSON, warnings, errors
printWarningsAndErrors :: Bool -> Bool -> P.MultipleErrors -> Either P.MultipleErrors a -> IO ()
printWarningsAndErrors verbose False warnings errors = do
  pwd <- getCurrentDirectory
  cc <- bool Nothing (Just P.defaultCodeColor) <$> ANSI.hSupportsANSI stderr
  let ppeOpts = P.defaultPPEOptions { P.ppeCodeColor = cc, P.ppeFull = verbose, P.ppeRelativeDirectory = pwd }
  -- when (P.nonEmpty warnings) $
  --   hPutStrLn stderr (P.prettyPrintMultipleWarnings ppeOpts warnings)
  case errors of
    Left errs -> do
      hPutStrLn stderr (P.prettyPrintMultipleErrors ppeOpts errs)
      return ()
      -- exitFailure
    Right _ -> return ()
printWarningsAndErrors verbose True warnings errors = do
  hPutStrLn stderr . BU8.toString . B.toStrict . A.encode $
    JSONResult (toJSONErrors verbose P.Warning warnings)
               (either (toJSONErrors verbose P.Error) (const []) errors)
  either (const exitFailure) (const (return ())) errors

printTime :: String -> IO ()
printTime tag = do
  zt <- getCurrentTime
  print $ tag <> ": " <> show zt

getModule :: BuildType -> [(FilePath, P.Module)] -> Maybe P.Module
getModule a b = do
  let fn = \f -> (find (\(filename, _) -> filename == f) b) >>= Just . snd
  case a of
    Add f -> fn f
    Modify f -> fn f
    _ -> Nothing

compileHelper :: PSCMakeOptions -> [FilePath] -> IORef [(FilePath, P.Module)] -> IORef [(P.ExternsFile, UTCTime)] -> IORef [P.ModuleName] -> C.MVar Int -> BuildType -> IO ()
compileHelper PSCMakeOptions{..} input arr externsRef errorModules loc buildType = do
  -- printTime "Start Compilation"
  a <- C.takeMVar loc
  hPutStrLn stdout "* Compilation started"
  (makeErrors, makeWarnings) <- runMake pscmOpts $ do
    case buildType of
      Boot -> do
        -- liftIO $ printTime "Parse modules start"
        moduleFiles <- liftIO $ readInput input
        ms <- P.parseModulesFromFiles id moduleFiles
        -- liftIO $ printTime "Parse modules end"
        liftIO $ writeIORef arr ms
      Add filename -> do
        moduleFiles <- liftIO $ readInput [filename]
        ms <- P.parseModulesFromFiles id moduleFiles
        liftIO $ modifyIORef arr (\a -> a <> ms)
      Modify filename -> do
        moduleFiles <- liftIO $ readInput [filename]
        ms <- P.parseModulesFromFiles id moduleFiles
        liftIO $ modifyIORef arr (\a -> (filter (\(file, _) -> file /= filename) a) <> ms)
      Remove filename -> liftIO $ do
        moduleCache <- readIORef arr
        externsCache <- readIORef externsRef
        let modu = find (\(file, _) -> file /= filename) moduleCache
        case modu of
          Just (_, module'@(P.Module _ _ moduleName _ _)) -> do
            modifyIORef externsRef  $ filter (\(P.ExternsFile{..}, _) -> moduleName /= efModuleName)
            modifyIORef arr $ filter (\(file, _) -> file /= filename)
          Nothing -> return ()
      _ -> return ()
    ms <- liftIO $ readIORef arr
    let filePathMap = M.fromList $ map (\(fp, P.Module _ _ mn _ _) -> (mn, Right fp)) ms
    foreigns <- inferForeignModules filePathMap
    let makeActions = buildMakeActions pscmOutputDir filePathMap foreigns pscmUsePrefix (Just externsRef) (Just errorModules)
    -- liftIO $ printTime "Make action start"
    P.make makeActions (map snd ms) (getModule buildType ms)
    -- liftIO $ printTime "Make action end"
  -- printTime "End Compilation"
  printWarningsAndErrors (P.optionsVerboseErrors pscmOpts) pscmJSONErrors makeWarnings makeErrors
  a <- C.putMVar loc a
  hPutStrLn stdout "* Compilation finished"

compile :: PSCMakeOptions -> IO ()
compile a@PSCMakeOptions{..} = do
  input <- globWarningOnMisses (unless pscmJSONErrors . warnFileTypeNotFound) pscmInput
  when (null input && not pscmJSONErrors) $ do
    hPutStr stderr $ unlines [ "purs compile: No input files."
                             , "Usage: For basic information, try the `--help' option."
                             ]
    exitFailure
  arr <- newIORef ([] :: [(FilePath, P.Module)])
  externsRef <- newIORef ([] :: [(P.ExternsFile, UTCTime)])
  errorModules <- newIORef ([] :: [P.ModuleName])
  watchLock <- C.newMVar 1
  let compilerFn = compileHelper a input arr externsRef errorModules watchLock
  let compileForiegnFn = \a b -> do
                            let pursFile = replaceExtension b "purs"
                            exists <- liftIO $ doesFileExist pursFile
                            if exists
                              then case a of
                                Add _ -> compilerFn (Modify pursFile)
                                Modify _ -> compilerFn (Modify pursFile)
                                Remove _ -> compilerFn (Modify pursFile)
                                _ -> compilerFn Boot
                              else compilerFn Boot
  let compileLamda = \a b -> if (".purs" `isSuffixOf` b) then (compilerFn a) else (compileForiegnFn a b)
  compilerFn Boot
  if pscmWatchSrc
    then
      withManagerConf (WatchConfig (Debounce 1.0) 1000 False) $ \mgr -> do
        watchTree
          mgr
          "src"
          (const True)
          (\ eventType -> do
              -- printInfo eventType
              case eventType of
                Added filename _ -> compileLamda (Add filename) filename
                Removed filename _ -> compileLamda (Remove filename) filename
                Modified filename _ -> compileLamda (Modify filename) filename
                _ -> compilerFn Boot
            )
        forever $ CC.threadDelay 1000000
    else do
      getLine >>= print
      exitSuccess
  -- where
  --   printInfo e = case e of
  --                   Added f _ -> hPutStrLn stdout ("* File Added: " <> f)
  --                   Removed f _ -> hPutStrLn stdout ("* File Removed: " <> f)
  --                   Modified f _ -> hPutStrLn stdout ("* File Modified: " <> f)
  --                   _ -> hPutStrLn stdout ("* Unknown file event occured")

warnFileTypeNotFound :: String -> IO ()
warnFileTypeNotFound = hPutStrLn stderr . ("purs compile: No files found using pattern: " ++)

globWarningOnMisses :: (String -> IO ()) -> [FilePath] -> IO [FilePath]
globWarningOnMisses warn = concatMapM globWithWarning
  where
  globWithWarning pattern' = do
    paths <- glob pattern'
    when (null paths) $ warn pattern'
    return paths
  concatMapM f = fmap concat . mapM f

readInput :: [FilePath] -> IO [(FilePath, Text)]
readInput inputFiles = forM inputFiles $ \inFile -> (inFile, ) <$> readUTF8FileT inFile

inputFile :: Opts.Parser FilePath
inputFile = Opts.strArgument $
     Opts.metavar "FILE"
  <> Opts.help "The input .purs file(s)"

outputDirectory :: Opts.Parser FilePath
outputDirectory = Opts.strOption $
     Opts.short 'o'
  <> Opts.long "output"
  <> Opts.value "output"
  <> Opts.showDefault
  <> Opts.help "The output directory"

comments :: Opts.Parser Bool
comments = Opts.switch $
     Opts.short 'c'
  <> Opts.long "comments"
  <> Opts.help "Include comments in the generated code"

verboseErrors :: Opts.Parser Bool
verboseErrors = Opts.switch $
     Opts.short 'v'
  <> Opts.long "verbose-errors"
  <> Opts.help "Display verbose error messages"

noPrefix :: Opts.Parser Bool
noPrefix = Opts.switch $
     Opts.short 'p'
  <> Opts.long "no-prefix"
  <> Opts.help "Do not include comment header"

jsonErrors :: Opts.Parser Bool
jsonErrors = Opts.switch $
     Opts.long "json-errors"
  <> Opts.help "Print errors to stderr as JSON"

sourceMaps :: Opts.Parser Bool
sourceMaps = Opts.switch $
     Opts.long "source-maps"
  <> Opts.help "Generate source maps"

dumpCoreFn :: Opts.Parser Bool
dumpCoreFn = Opts.switch $
     Opts.long "dump-corefn"
  <> Opts.help "Dump the (functional) core representation of the compiled code at output/*/corefn.json"

watchSrc :: Opts.Parser Bool
watchSrc = Opts.switch $
     Opts.long "watchSrc"
  <> Opts.help "Watch for code changes in ./src directory"

options :: Opts.Parser P.Options
options = P.Options <$> verboseErrors
                    <*> (not <$> comments)
                    <*> sourceMaps
                    <*> dumpCoreFn

pscMakeOptions :: Opts.Parser PSCMakeOptions
pscMakeOptions = PSCMakeOptions <$> many inputFile
                                <*> outputDirectory
                                <*> options
                                <*> (not <$> noPrefix)
                                <*> jsonErrors
                                <*> watchSrc

command :: Opts.Parser (IO ())
command = compile <$> (Opts.helper <*> pscMakeOptions)
